//! An addtive wavetable synthesizer built in rust.

#![license = "MIT"]

extern crate libc;
extern crate portaudio;

pub mod waveform;
pub mod wavetable;
pub mod oscillator;
pub mod sink;
pub mod port_audio_sink;
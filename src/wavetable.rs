use std::io::{File, Open, ReadWrite};
use std::fmt;
use std::path::posix::Path;

use oscillator::Oscillator;
use waveform::{Waveform};

static SAMPLES: uint = 2048;
static HARMONICS: uint = 256;

/// Describes multiple types of Precision that are
/// availabled during indexing samples in a wavetable
pub enum WavetablePrecision {
     Aliased,
     Linear
}

// Holds all the wavetable data for any given waveform
// Used to query specific amplitudes for specific waveforms
// at specific steps in order to generate sound.
pub struct Wavetable {
       table: ::std::vec::Vec<f32>,
       samples: uint,
       precision: WavetablePrecision       
}

impl Wavetable {

     // Creates and returns a new wavetable.
     pub fn new(precision: WavetablePrecision, samples: uint) -> Wavetable { 
        return Wavetable{table: vec!(), precision: precision, samples: samples};
     }

     // Retrieves the desired sample at the passed in step
     // by using the configured aliasing strategy.
     pub fn get_sample(&self, step: f32) -> f32 {
        match self.precision {
          Aliased => self.direct_sample(step),
          Linear => self.linear_interpolate(step),
        }
     }

     // This function gets the direct sample from the passed in step.
     // The step is floored such that it can actually be indexed into an array. 
     fn direct_sample(&self, step: f32) -> f32 {
       let index : uint = ::std::num::cast(step.floor()).unwrap();
       return *self.table.get(index);
     }

     // This function attempts to linerally interpolate the passed in step
     // By drawing a line between the nearest two indicies and attempting
     // to approximate the value of the sample.
     fn linear_interpolate(&self, step: f32) -> f32 {
       let a : uint = ::std::num::cast(step.floor()).unwrap();
       let mut b : uint = ::std::num::cast(step.ceil()).unwrap();

       if b >= SAMPLES {
         b -= SAMPLES;
       }

       let y1 = *self.table.get(a);
       let y2 = *self.table.get(b);

       let slope = y2 - y1;
       let intersect = y1 - (slope * ::std::num::cast(a).unwrap());

       return slope * step + intersect;
     }

     // Writes the body of the wavetable to a new line seperated file.
     // Creates or appends to the passed in filename.
     // Primarily used for debugging and visualization purposes.
     pub fn write_to_file(&self, path: &Path) {
       let mut handle = File::open_mode(path, Open, ReadWrite).unwrap();
       
       for sample in self.table.iter() {
         let payload = format_args!(fmt::format, "{}\n", *sample).to_string();
         let res = handle.write(payload.into_bytes().as_slice());
         println!("{}", res);
       }
     }

     pub fn fill(&mut self, waveform: Waveform) {
       let osc = Oscillator::new(waveform, HARMONICS);
       for n in range(0, self.samples) {
         let phase: f32 = ::std::num::cast(n).unwrap();
         self.table.push(osc.generate_sample(1.0, phase));
       }

       let first = *(self.table).get(0);
       self.table.push(first);
     }
 
     pub fn step_size(&self, freq: f32) -> f32 {
       let samples_float : f32 = ::std::num::cast(self.samples).unwrap();
       return samples_float * freq / 44100.0;
     }
}
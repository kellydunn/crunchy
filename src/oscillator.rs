use std::num;
use waveform::{Waveform, Sine, Saw, Square, Triangle};

static SAMPLES: f32 = 2048.0;

/// An oscillator generates singals based of of the four standard Fourier Series:
///   - Sine
///   - Saw
///   - Square
///   - Triangle
///
/// It can be configured to generate samples of waveforms for up to a maximum number of harmonic frequencies
/// which is controlled by the `harmonics` attribute.
pub struct Oscillator {
     waveform: Waveform,
     harmonics: uint
}

impl Oscillator {

     /// Creates and returns a new Oscillator
     /// of the passed in Waveform type, generating up to the number of passed in harmonic frequencies
     pub fn new(waveform: Waveform, harmonics: uint) -> Oscillator {
        return Oscillator{waveform: waveform, harmonics: harmonics};
     }

     /// Returns a single f32 sample of the current Oscillator for the passed in frequency and phase.
     pub fn generate_sample(&self, freq: f32, phase: f32) -> f32 {
        match self.waveform {
          Sine => self.sine(freq, phase),
          Saw => self.saw(freq, phase),
          Square => self.square(freq, phase),
          Triangle => self.triangle(freq, phase)
        }
     }

     /// Returns a single sine wave sample for the passed in frequency and phase.
     fn sine(&self, freq: f32, phase: f32) -> f32 {
        return ((2.0 * Float::pi() * freq * phase) / SAMPLES).sin();
     }

     /// Returns a single saw wave sample for the passed in frequency and phase.
     fn saw(&self, freq: f32, phase: f32) -> f32 {
        let mut sum : f32 = 0.0;
        for i in range(1, self.harmonics) {
          let harmonic : f32 = ::std::num::cast(i).unwrap();
          let base : f32 = -1.0;
          let exponent : uint = 1 + ::std::num::cast(harmonic).unwrap();
          let power : f32 = ::std::num::pow(base, exponent);
          sum += power * self.sine(freq * harmonic, phase) / harmonic;
        }

        return (2.0/Float::pi()) * sum;
     }

     /// Returns a single square wave sample for the passed in frequency and phase.
     fn square(&self, freq: f32, phase: f32) -> f32 {
        let mut sum = 0.0;
        for i in range(1, self.harmonics) {
          let harmonic : f32 = ::std::num::cast(i).unwrap();
          let coeff : f32 = 1.0/harmonic;
          if i % 2 == 1 {
            sum += coeff * self.sine(freq * harmonic, phase);
          }
        }

        return sum;
     }

     /// Returns a single triangle wave sample for the passed in frequency and phase.
     fn triangle(&self, freq: f32, phase: f32) -> f32 {
        let mut sum = 0.0;
        let mut switch = true;

        for i in range(1, self.harmonics) {
          let harmonic : f32 = ::std::num::cast(i).unwrap();

          if i % 2 == 1 {
            let denominator = ::std::num::cast(num::pow(i, 2)).unwrap();
            let val = (1.0/denominator) * self.sine(harmonic * freq, phase);

            if switch {
               sum +=  val;
            } else {
               sum -= val;
            }

            switch = !switch;
          }
        }

       return (8.0/(num::pow(Float::pi(), 2))) * sum;
     }

  }  


#[cfg(test)]
mod test {
  use oscillator;
  use std::f32;
  use std::string::{String};
  use waveform::{Sine, Saw, Square, Triangle};

  #[test]
  fn test_sine() {
    let osc = oscillator::Oscillator::new(Sine, 256); 
    let mut res = osc.sine(440.0, 0.0);
 
    if res != 0.0 {
      fail!("Unexpected value {} for first phase of sine function", res);
    }

    res = osc.sine(440.0, 0.1);
    if f32::to_str_exact(res, 6) != String::from_str("0.134581") {
      fail!("Unexpected value {} for phase 0.1 of sine function", res);
    }
  }

  #[test]
  fn test_saw() {
    let osc = oscillator::Oscillator::new(Saw, 256); 
    let mut res = osc.saw(440.0, 0.0);
 
    if res != 0.0 {
      fail!("Unexpected value {} for first phase of saw function", res);
    }

    res = osc.saw(440.0, 0.1);
    if f32::to_str_exact(res, 6) != String::from_str("0.043053") {
      fail!("Unexpected value {} for phase 0.1 of saw function", res);
    }
  }

  #[test]
  fn test_square() {
    let osc = oscillator::Oscillator::new(Square, 256); 
    let mut res = osc.square(440.0, 0.0);
 
    if res != 0.0 {
      fail!("Unexpected value {} for first phase of square function", res);
    }

    res = osc.square(440.0, 0.1);
    if f32::to_str_exact(res, 6) != String::from_str("0.799887") {
      fail!("Unexpected value {} for phase 0.1 of square function", res);
    }
  }

  #[test]
  fn test_triangle() {
    let osc = oscillator::Oscillator::new(Triangle, 256); 
    let mut res = osc.triangle(440.0, 0.0);
 
    if res != 0.0 {
      fail!("Unexpected value {} for first phase of triangle function", res);
    }

    res = osc.triangle(440.0, 0.1);
    if f32::to_str_exact(res, 6) != String::from_str("0.085938") {
      fail!("Unexpected value {} for phase 0.1 of triangle function", res);
    }
  }
}
use std;
use sink::{Sink};

use portaudio::pa;
use portaudio::types;

static FRAMES_PER_BUF: u32 = 1;
static SAMPLE_RATE: f64 = 44100.0;

/// A Port Audio Sink handles passing audio data
/// to a port audio sound device for the purposes of making it audible.
pub struct PortAudioSink {
  stream: pa::PaStream<f32>
}

impl PortAudioSink {

    /// Creates and returns a new port audio sink 
    /// configured from the default input and output devices
    /// described by portaudio.
    pub fn new() -> PortAudioSink {

      pa::initialize();
      let mut stream : pa::PaStream<f32> = pa::PaStream::new(types::PaFloat32); 
      let default_input = pa::get_default_input_device();
      let default_output = pa::get_default_output_device();
      let output_info = pa::get_device_info(default_output).unwrap();

      println!("Default output device: {}", output_info.name);
      println!("Default output sample rate: {}", output_info.default_sample_rate);

      let stream_params = types::PaStreamParameters {
        device: default_input,
        channel_count: 2,
        sample_format: types::PaFloat32,
        suggested_latency: pa::get_device_info(default_input).unwrap().default_low_output_latency
      };

      let stream_params_out = types::PaStreamParameters {
        device: default_output,
        channel_count: 2,
        sample_format: types::PaFloat32,
        suggested_latency: pa::get_device_info(default_output).unwrap().default_low_output_latency
      };

      let mut err = stream.open(Some(&stream_params), Some(&stream_params_out), SAMPLE_RATE, FRAMES_PER_BUF, types::PaClipOff);
      println!("Portaudio Open error: {}", pa::get_error_text(err));

      err = stream.start();
      println!("Portaudio Start error: {}", pa::get_error_text(err));
      
      let pasink = PortAudioSink{stream: stream};
      return pasink;
  }
}

impl Sink for PortAudioSink {

     /// Implements the Sink trait for the Port Audio Sink
     /// which sinks audio data to the current Port Audio Enabled Device
     fn sink(&self, data: std::vec::Vec<f32>) {
       self.stream.write(data, FRAMES_PER_BUF);
     }

}
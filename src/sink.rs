use std;

/// Sinks describe how to write audio samples to audio devices
pub trait Sink {

    /// Writes the passed in audio data to the implemented Sink
    fn sink(&self, std::vec::Vec<f32>);

}
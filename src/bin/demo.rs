extern crate crunchy;
extern crate time;
extern crate libc;

use crunchy::waveform::{Sine, Square, Saw, Triangle};
use crunchy::wavetable::{Wavetable, Linear};
use crunchy::sink::{Sink};
use crunchy::port_audio_sink::{PortAudioSink};

static SAMPLES: uint = 2048;
static SAMPLES_FLOAT: f32 = 2048.0;
static NYQUIST: f32 = 20000.0;
static FREQ: f32 = 440.0;
static VOLUME: f32 = 0.4;

fn main() {
    let sink = PortAudioSink::new();

    // Build the wavetable for 1hz
    let mut sine_wavetable : Wavetable = Wavetable::new(Linear, SAMPLES);
    let mut saw_wavetable : Wavetable = Wavetable::new(Linear, SAMPLES);
    let mut square_wavetable : Wavetable = Wavetable::new(Linear, SAMPLES);
    let mut triangle_wavetable :  Wavetable = Wavetable::new(Linear, SAMPLES);

    sine_wavetable.fill(Sine);
    saw_wavetable.fill(Saw);
    square_wavetable.fill(Square);
    triangle_wavetable.fill(Triangle);

    sine_wavetable.write_to_file(&Path::new("test/sine_wavetable"));
    saw_wavetable.write_to_file(&Path::new("test/saw_wavetable"));
    square_wavetable.write_to_file(&Path::new("test/square_wavetable"));
    triangle_wavetable.write_to_file(&Path::new("test/triangle_wavetable"));

    // Start sampling
    let mut count : f32 = 0.0;
    let mut sample;
    let mut freq = FREQ;
    let mut index = 0;
    let tables = [sine_wavetable, 
                  saw_wavetable, 
                  square_wavetable, 
                  triangle_wavetable];

    loop {
         sample = tables[index].get_sample(count);
         sink.sink(vec!(VOLUME * sample));

         count += tables[index].step_size(freq);
         if freq < NYQUIST {
           freq += 0.1;
         } else {
           if index > tables.len() {
             break;
           } else {
             freq = 1.0;
             index += 1;
           }
         }

         if count >= SAMPLES_FLOAT {
           count -= SAMPLES_FLOAT;
         }
    }   
}

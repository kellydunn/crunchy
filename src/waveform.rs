/// Crunchy currently supports the following four standard Fourier Series Waveforms:
///   - Sine
///   - Saw
///   - Square
///   - Triangle
pub enum Waveform {
     Sine,
     Saw,
     Square,
     Triangle
}